package com.service;


import com.service.encryption.Encrypt;
import org.junit.Assert;
import org.junit.Test;

public class EncryptTest {


    @Test
    public void encryptTest() {
        Encrypt e = new Encrypt();
        String password = "HNotifier2018";
        String encryptPassword = "Pl7Oz3PZTYmENOr4n0tk1w==";
        Assert.assertEquals(encryptPassword, e.encode(password));
    }

    @Test
    public void passwordVerificationTest() {
        Encrypt e = new Encrypt();
        String inputPass = "HNotifier2018";
        String passwordFromDB = "Pl7Oz3PZTYmENOr4n0tk1w==";
//        String inputPass = null;
//        String passwordFromDB = null;
        Assert.assertEquals(true, e.matches(inputPass, passwordFromDB));
    }
}
