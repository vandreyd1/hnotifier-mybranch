package com.dao;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import com.exception.LocationNotSpecifiedException;
import com.model.Location;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:/test_spring-application-context.xml")
@SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = "/db/migration/V1__initial_tables.sql")
})
@Transactional
public class LocationDaoImplTest {

  @Autowired
  private LocationDao locationDao;

  private final String branchOfficeName = "branch office name";
  private final String cityName = "odessa city";
  private final String updatedCityName = "odessa city - updated";
  private Location location;
  private Location location2;

  @Before
  public void init() throws Exception {
    location = new Location();
    location.setBranchOfficeName(branchOfficeName);
    location.setCity(cityName);

    location2 = new Location();
  }

  @Test
  public void shouldSaveLocationAndThenGetByIdAndByOfficeName() throws Exception {
    location = locationDao.save(location);

    Optional<Location> foundLocation = locationDao.findById(location.getId());
    assertThat(foundLocation.get().getCity(), equalTo(cityName));

    List<Location> byCityNameList = locationDao.findByCityName(cityName);
    assertThat(byCityNameList.size(), equalTo(1));
    assertThat(byCityNameList.get(0).getCity(), equalTo(cityName));

    List<Location> locationList = locationDao.findByBranchName(branchOfficeName);
    assertThat(locationList.size(), equalTo(1));
    assertThat(locationList.get(0).getBranchOfficeName(), equalTo(branchOfficeName));
    assertThat(locationList.get(0).getCity(), equalTo(cityName));
  }

  @Test
  public void shouldUpdateCityName(){
    location = locationDao.save(location);
    location.setCity(updatedCityName);
    location = locationDao.update(location);
    List<Location> updatedCityNameList = locationDao.findByCityName(updatedCityName);
    assertThat(updatedCityNameList.size(), equalTo(1));
    assertThat(updatedCityNameList.get(0).getCity(), equalTo(updatedCityName));
  }

  @Test(expected = LocationNotSpecifiedException.class)
  public void souladThrowNotFoundLocationException() {
    locationDao.findById(null);
  }

  @Test
  public void shouldDeleteLocation() {
    location = locationDao.save(location);
    locationDao.delete(location);

    Optional<Location> byId = locationDao.findById(location.getId());
    assertFalse("should return false", byId.isPresent());
  }

  @Test
  public void shouldReturnListOfTwoLocations() {
    location = locationDao.save(location);
    location2 = locationDao.save(location2);

    List<Location> locations = locationDao.findAll();
    assertEquals(2, locations.size());
  }
}