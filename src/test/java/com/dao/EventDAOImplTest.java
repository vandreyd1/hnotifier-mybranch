package com.dao;

import static org.assertj.core.api.Assertions.assertThat;

import com.model.Event;
import com.model.EventType;
import com.model.Location;
import com.model.Person;
import com.model.SubscriptorEvent;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/test_spring-application-context.xml")
@SqlGroup({
    @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = "/db/migration/V1__initial_tables.sql")
})
@Transactional
public class EventDAOImplTest {

  @Autowired
  private EventDAO eventDAO;
  @Autowired
  private EventTypeDao eventTypeDao;
  @Autowired
  private LocationDao locationDao;
  @Autowired
  private PersonDAO personDAO;

  private Event event;
  private EventType eventType;
  private Location location;
  private Person person;

  private final String eventText = "some event";
  private final String updatedEventText = "some event updated";
  private final String eventTypeName = "webinar";
  private final String cityName = "Odessa";

  @Before
  public void init() {
    event = new Event();
    event.setText(eventText);
    event.setDateTime(LocalDateTime.now());

    eventType = new EventType();
    eventType.setName(eventTypeName);

    location = new Location();
    location.setCity(cityName);

    person = new Person();
    person.setFirstName("sergii");
  }

  @Test
  public void shouldSaveAndGetBackById() throws Exception {
    eventType = eventTypeDao.save(eventType);
    eventType.addEvent(event);
    event = eventDAO.save(event);

    Optional<Event> foundId = eventDAO.findById(event.getId());
    assertThat(foundId.isPresent()).isTrue();
    assertThat(foundId.get().getText()).isEqualTo(eventText);
  }

  @Test
  public void shouldSaveAndGetBackByLocation() {
    eventType = eventTypeDao.save(eventType);
    location = locationDao.save(location);
    eventType.addEvent(event);
    event.setLocation(location);
    eventDAO.save(event);

    List<Event> foundEvents = eventDAO.getAllByLocation(location);
    assertThat(foundEvents.size()).isEqualTo(1);
    assertThat(foundEvents.get(0).getText()).isEqualTo(eventText);
  }

  @Test
  public void shouldSaveAndGetBackByLocationAndBetweenDates() {
    eventType = eventTypeDao.save(eventType);
    location = locationDao.save(location);
    eventType.addEvent(event);
    event.setLocation(location);
    event = eventDAO.save(event);

    LocalDateTime from = LocalDate.now().atStartOfDay().minusDays(1);
    LocalDateTime to = LocalDate.now().atStartOfDay().plusDays(1);
    List<Event> foundEvents = eventDAO.getAllBetweenTimeAndLocation(from, to, location);
    assertThat(foundEvents.size()).isEqualTo(1);
    assertThat(foundEvents.get(0).getText()).isEqualTo(eventText);

    from = LocalDate.now().atStartOfDay().plusDays(1);
    to = LocalDate.now().atStartOfDay().plusDays(2);
    foundEvents = eventDAO.getAllBetweenTimeAndLocation(from, to, location);
    assertThat(foundEvents.isEmpty()).isTrue();
  }

  @Test
  public void shouldSaveAndGetBackByLocationForToday() {
    eventType = eventTypeDao.save(eventType);
    location = locationDao.save(location);
    eventType.addEvent(event);
    event.setLocation(location);
    event = eventDAO.save(event);

    List<Event> foundEvents = eventDAO.getAllTodayByLocation(location);
    assertThat(foundEvents.isEmpty()).isFalse();
    assertThat(foundEvents.get(0).getText()).isEqualTo(eventText);

    event.setDateTime(LocalDateTime.now().plusDays(1));
    eventDAO.save(event);

    foundEvents = eventDAO.getAllTodayByLocation(location);
    assertThat(foundEvents.isEmpty()).isTrue();
  }

  @Test
  public void shouldSaveAndGetBackAllForToday() {
    eventType = eventTypeDao.save(eventType);
    location = locationDao.save(location);
    eventType.addEvent(event);
    event.setLocation(location);
    event = eventDAO.save(event);

    List<Event> foundEvents = eventDAO.getAllToday();
    assertThat(foundEvents.isEmpty()).isFalse();
    assertThat(foundEvents.get(0).getText()).isEqualTo(eventText);

    event.setDateTime(LocalDateTime.now().plusDays(1));
    eventDAO.save(event);

    foundEvents = eventDAO.getAllToday();
    assertThat(foundEvents.isEmpty()).isTrue();
  }

  @Test
  public void shouldSaveAndGetBackByLocationAndFuture() {
    eventType = eventTypeDao.save(eventType);
    location = locationDao.save(location);
    eventType.addEvent(event);
    event.setLocation(location);
    event = eventDAO.save(event);

    List<Event> foundEvents = eventDAO.getAllFutureByLocation(location);
    assertThat(foundEvents.isEmpty()).isTrue();

    event.setDateTime(LocalDateTime.now().plusDays(1));
    eventDAO.save(event);

    foundEvents = eventDAO.getAllFutureByLocation(location);
    assertThat(foundEvents.isEmpty()).isFalse();
    assertThat(foundEvents.get(0).getText()).isEqualTo(eventText);
  }

  @Test
  public void shouldSaveAndGetBackForFuture() {
    eventType = eventTypeDao.save(eventType);
    location = locationDao.save(location);
    eventType.addEvent(event);
    event.setLocation(location);
    event = eventDAO.save(event);

    List<Event> foundEvents = eventDAO.getAllFuture();
    assertThat(foundEvents.isEmpty()).isTrue();

    event.setDateTime(LocalDateTime.now().plusDays(1));
    eventDAO.save(event);

    foundEvents = eventDAO.getAllFuture();
    assertThat(foundEvents.isEmpty()).isFalse();
    assertThat(foundEvents.get(0).getText()).isEqualTo(eventText);
  }

  @Test
  public void shouldSaveAndDelete() {
    eventType = eventTypeDao.save(eventType);
    location = locationDao.save(location);
    eventType.addEvent(event);
    event.setLocation(location);
    event = eventDAO.save(event);

    Optional<Event> foundId = eventDAO.findById(event.getId());
    assertThat(foundId.isPresent()).isTrue();

    eventDAO.delete(event);

    foundId = eventDAO.findById(event.getId());
    assertThat(foundId.isPresent()).isFalse();
  }

  @Test
  public void shouldSaveAndUpdate() {
    eventType = eventTypeDao.save(eventType);
    location = locationDao.save(location);
    eventType.addEvent(event);
    event.setLocation(location);
    event = eventDAO.save(event);

    Optional<Event> foundEvent = eventDAO.findById(event.getId());
    assertThat(foundEvent.isPresent()).isTrue();
    assertThat(foundEvent.get().getText()).isEqualTo(eventText);

    event.setText(updatedEventText);
    eventDAO.update(event);

    foundEvent = eventDAO.findById(event.getId());
    assertThat(foundEvent.isPresent()).isTrue();
    assertThat(foundEvent.get().getText()).isEqualTo(updatedEventText);
  }

  @Test
  public void shouldReturnSubscriber() {

    eventType = eventTypeDao.save(eventType);
    location = locationDao.save(location);
    person.setLocation(location);
    person = personDAO.save(person);

    eventType.addEvent(event);
    event.setLocation(location);
    event = eventDAO.save(event);

    event.addSubscriptor(person);
    eventDAO.save(event);

    Optional<Event> foundEvent = eventDAO.findById(event.getId());
    assertThat(foundEvent.isPresent()).isTrue();
    Set<SubscriptorEvent> subscriptorEvents = foundEvent.get().getSubscriptorEvents();
    assertThat(subscriptorEvents.size()).isEqualTo(1);
    assertThat(subscriptorEvents.iterator().next().getPerson()).isEqualTo(person);
  }
}