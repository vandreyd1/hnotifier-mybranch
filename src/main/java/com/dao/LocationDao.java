package com.dao;

import com.model.Location;

import java.util.List;
import java.util.Optional;

public interface LocationDao {

  Optional<Location> findById(Integer id);

  List<Location> findAll();

  List<Location> findByCityName(String cityName);

  List<Location> findByBranchName(String branchName);

  Location update(Location location);

  void delete(Location location);

  boolean delete(Integer locationId);

  Location save(Location location);
}
