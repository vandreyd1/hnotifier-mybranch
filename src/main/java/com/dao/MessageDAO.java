package com.dao;

import com.model.Message;

public interface MessageDAO {

  Message save(Message message);
}
