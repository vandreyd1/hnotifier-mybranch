package com.dao.impl;

import com.dao.PersonDAO;
import com.dao.WebAppEntityDAO;
import com.model.WebAppEntity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class WebAppEntityDAOImpl implements WebAppEntityDAO {
  @Autowired
  PersonDAO personDAO;

  private List<WebAppEntity> entities = Arrays.asList(
      new WebAppEntity(1, "name1"),
      new WebAppEntity(2, "name2"),
      new WebAppEntity(3, "name3"));


  public List getAllEntities() {
    return personDAO.findAll();
  }
}
