package com.dao.impl;

import com.dao.PersonDAO;
import com.exception.PersonNotFoundException;
import com.model.Location;
import com.model.Person;
import com.model.Person.Comparators;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Transactional(readOnly = true)
@Repository
public class PersonDAOImpl implements PersonDAO {

  @PersistenceContext
  private EntityManager em;

  @Transactional(readOnly = false)
  @Override
  public Person save(Person person) {
    return em.merge(person);
  }

  @Transactional(readOnly = false)
  @Override
  public void delete(Person person) {
    Person foundPerson = findById(person.getId()).orElseThrow(() -> new PersonNotFoundException());
    em.remove(foundPerson);
  }

  @Transactional(readOnly = false)
  @Override
  public void update(Person person) {
    em.merge(person);
  }

  @Override
  public Optional<Person> findById(Integer id) {
    if (id == null) {
      throw new PersonNotFoundException();
    }
    Person person = em.find(Person.class, id);
    return Optional.ofNullable(person);
  }

  @Override
  public List<Person> findAll() {
    List<Person> resultList = em.createQuery(
            "select p from Person p").getResultList();
    Collections.sort(resultList, Comparators.LASTNAME);
    return resultList;
  }

  @Override
  public Optional<Person> findByLogin(String login) {
    List<Person> resultList = em.createQuery("select p from Person p where p.login = ?1",
            Person.class)
            .setParameter(1, login)
            .getResultList();
    Person person = null;
    if (!resultList.isEmpty()) {
      person = resultList.get(0);
    }
    return Optional.ofNullable(person);
  }

  @Override
  public List<Person> findAllByLocation(Location location) {
    List<Person> resultList = em.createQuery("select p from Person p where p.location = ?1",
            Person.class)
            .setParameter(1, location)
            .getResultList();
    Collections.sort(resultList, Comparators.LASTNAME);
    return resultList;
  }

  @Override
  public List<Person> findByLastName(String lastName) {
    List<Person> resultList = em.createQuery("select p from Person p where p.lastName = ?1",
            Person.class)
            .setParameter(1, lastName)
            .getResultList();
    Collections.sort(resultList, Comparators.LASTNAME);
    return resultList;
  }

  public EntityManager getEm() {
    return em;
  }

  public void setEm(EntityManager em) {
    this.em = em;
  }

}
