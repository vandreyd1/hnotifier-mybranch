package com.dao.impl;

import com.dao.*;
import com.model.*;
import java.util.*;
import javax.persistence.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

@Transactional(readOnly = true)
@Repository
public class RoleDAOImpl implements RoleDAO {


  @PersistenceContext
  private EntityManager em;

  @Override
  public List<Role> findAll() {
    List<Role> resultList = em.createQuery(
        "select r from Role r").getResultList();
    return resultList;
  }
}
