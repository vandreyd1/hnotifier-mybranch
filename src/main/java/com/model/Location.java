package com.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOCATION")
public class Location {

  @Id
  @SequenceGenerator(sequenceName = "location_id_sequence", name = "locationIdSequence")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "locationIdSequence")
  @Column(name = "ID")
  private Integer id;

  @Column(name = "CITY", length = 255)
  private String city;

  @Column(name = "BRANCH_OFFICE_NAME", length = 255)
  private String branchOfficeName;

  @Column(name = "ADDRESS", length = 255)
  private String address;

  @OneToMany(mappedBy = "location")
  private Set<Person> people = new HashSet<>();

  @OneToMany(mappedBy = "location", orphanRemoval = true,
      cascade = {CascadeType.REMOVE})
  private Set<Event> events = new HashSet<>();

  @ManyToMany(mappedBy = "locations")
  private Set<StudyGroup> studyGroups = new HashSet<>();


  public void addPerson(Person person) {
    this.people.add(person);
    person.setLocation(this);
  }

  public void removePerson(Person person) {
    this.people.remove(person);
    person.setLocation(null);
  }

  public void addEvent(Event event) {
    this.events.add(event);
    event.setLocation(this);
  }

  public void removeLocation(Event event) {
    this.events.add(event);
    event.setLocation(null);
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getBranchOfficeName() {
    return branchOfficeName;
  }

  public void setBranchOfficeName(String branchOfficeName) {
    this.branchOfficeName = branchOfficeName;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Set<Person> getPeople() {
    return people;
  }

  public void setPeople(Set<Person> people) {
    this.people = people;
  }

  public Set<Event> getEvents() {
    return events;
  }

  public void setEvents(Set<Event> events) {
    this.events = events;
  }

  public Set<StudyGroup> getStudyGroups() {
    return studyGroups;
  }

  public void setStudyGroups(Set<StudyGroup> studyGroups) {
    this.studyGroups = studyGroups;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Location location = (Location) obj;
    return Objects.equals(city, location.city)
        && Objects.equals(branchOfficeName, location.branchOfficeName)
        && Objects.equals(address, location.address);
  }

  @Override
  public int hashCode() {
    return Objects.hash(city, branchOfficeName, address);
  }
}
