package com.model;


public class Email {
    private String recipientAddress;
    private String subject;
    private String message;

    public Email() { }

    public Email(String recipientAddress, String subject, String message) {
        this.recipientAddress = recipientAddress;
        this.subject = subject;
        this.message = message;
    }

    public String getRecipientAddress() {
        return recipientAddress;
    }

    public void setRecipientAddress(String recipientAddress) {
        this.recipientAddress = recipientAddress;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
