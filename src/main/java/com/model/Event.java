package com.model;

import com.domain.LocalDateTimeAttributeConverter;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EVENT")
public class Event {

  @Id
  @SequenceGenerator(sequenceName = "event_id_sequence", name = "eventIdSequence")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eventIdSequence")
  @Column(name = "ID")
  private Integer id;

  @Column(name = "DATE_TIME", columnDefinition = "timestamp")
  @Convert(converter = LocalDateTimeAttributeConverter.class)
  private LocalDateTime dateTime;

  @Column(name = "DURATION")
  private Duration duration;

  @Column(name = "TEXT", nullable = true, length = 500)
  private String text;

  @ManyToOne
  @JoinColumn(name = "EVENT_TYPE_ID")
  private EventType eventType;

  @ManyToOne
  @JoinColumn(name = "LOCATION_ID")
  private Location location;

  @OneToMany(mappedBy = "event", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<SubscriptorEvent> subscriptorEvents = new HashSet<>();

  public void addSubscriptor(Person person) {
    SubscriptorEvent subscriptorEvent = new SubscriptorEvent(this, person);
    this.subscriptorEvents.add(subscriptorEvent);
    person.getSubscriptorEvents().add(subscriptorEvent);
  }

  public void removeSubscriptor(Person person) {
    SubscriptorEvent subscriptorEvent = new SubscriptorEvent(this, person);
    this.subscriptorEvents.remove(subscriptorEvent);
    subscriptorEvent.setEvent(null);
    person.getSubscriptorEvents().remove(subscriptorEvent);
  }

  public Set<SubscriptorEvent> getSubscriptorEvents() {
    return subscriptorEvents;
  }

  public void setSubscriptorEvents(Set<SubscriptorEvent> subscriptorEvents) {
    this.subscriptorEvents = subscriptorEvents;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public LocalDateTime getDateTime() {
    return dateTime;
  }

  public void setDateTime(LocalDateTime dateTime) {
    this.dateTime = dateTime;
  }

  public Duration getDuration() {
    return duration;
  }

  public void setDuration(Duration duration) {
    this.duration = duration;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public EventType getEventType() {
    return eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    Event event = (Event) obj;
    return Objects.equals(id, event.id)
        && Objects.equals(dateTime, event.dateTime)
        && Objects.equals(duration, event.duration)
        && Objects.equals(text, event.text);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dateTime, duration, text);
  }
}
