package com.model;

import com.annotations.*;
import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "PERSON")
public class Person {

    @Id
    @SequenceGenerator(sequenceName = "person_id_sequence", name = "personIdSequence")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personIdSequence")
    @Column(name = "ID")
    private Integer id;

    @Pattern(regexp = "^[a-zA-Z][a-zA-Z-_\\. ]{2,20}$")
    @Column(name = "FIRST_NAME", length = 255)
    private String firstName;

    @Pattern(regexp = "^[a-zA-Z][a-zA-Z-_\\. ]{2,20}$")
    @Column(name = "LAST_NAME", length = 255)
    private String lastName;

    @Phone(message = "Phone")
    @Column(name = "PHONE_NUMBER", length = 255)
    private String phoneNumber;

    @Email(regexp = "^([a-z0-9_\\.-]+)@([a-z0-9][a-z0-9_\\.-]+)\\.([a-z\\.]{2,6})$")
    @Column(name = "LOGIN", length = 255)
    private String login;

    @Pattern(message = "Wrong.password", regexp = "(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$")
    @Column(name = "PASSWORD", length = 45)
    private String password;

    @Transient
    private String passwordConfirm;

    @ManyToOne
    @JoinColumn(name = "LOCATION_ID")
    private Location location;

    @OneToMany(mappedBy = "person", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<SubscriptorEvent> subscriptorEvents = new HashSet<>();

    @ManyToMany(mappedBy = "people")
    private Set<StudyGroup> studyGroups = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "PERSON_ROLE",
            joinColumns = @JoinColumn(name = "PERSON_ID"),
            inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private Set<Role> roles = new HashSet<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Set<SubscriptorEvent> getSubscriptorEvents() {
        return subscriptorEvents;
    }

    public void setSubscriptorEvents(Set<SubscriptorEvent> subscriptorEvents) {
        this.subscriptorEvents = subscriptorEvents;
    }

    public Set<StudyGroup> getStudyGroups() {
        return studyGroups;
    }

    public void setStudyGroups(Set<StudyGroup> studyGroups) {
        this.studyGroups = studyGroups;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Person person = (Person) obj;
        return Objects.equals(firstName, person.firstName)
                && Objects.equals(lastName, person.lastName)
                && Objects.equals(phoneNumber, person.phoneNumber)
                && Objects.equals(login, person.login)
                && Objects.equals(password, person.password)
                && Objects.equals(location, person.location);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, phoneNumber, login, password, location);
    }


    public static class Comparators {

        public static final Comparator<Person> LASTNAME =
                (Person o1, Person o2) -> o1.getLastName().compareToIgnoreCase(o2.getLastName());

        public static final Comparator<Person> FIRSTNAME =
                (Person o1, Person o2) -> o1.getFirstName().compareToIgnoreCase(o2.getFirstName());

        public static final Comparator<Person> LOGIN =
                (Person o1, Person o2) -> o1.getLogin().compareToIgnoreCase(o2.getLogin());
    }
}
