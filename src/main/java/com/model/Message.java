package com.model;

import com.domain.LocalDateAttributeConverter;

import java.time.LocalDate;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MESSAGE")
public class Message {

  @Id
  @SequenceGenerator(sequenceName = "message_id_sequence", name = "messageIdSequence")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "messageIdSequence")
  @Column(name = "ID", nullable = false, unique = true)
  private Long id;

  @ManyToOne
  @JoinColumn(name = "SENDER_ID", nullable = false, updatable = false)
  private Person sender;

  @ManyToOne
  @JoinColumn(name = "RECEIVER_ID", nullable = false, updatable = false)
  private Person receiver;

  @Column(name = "IS_READ", nullable = false)
  private Boolean isRead;

  @Column(name = "DATE", nullable = false, columnDefinition = "date")
  @Convert(converter = LocalDateAttributeConverter.class)
  private LocalDate date;

  @Column(name = "CONTENT", nullable = false)
  private String content;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Person getSender() {
    return sender;
  }

  public void setSender(Person sender) {
    this.sender = sender;
  }

  public Person getReceiver() {
    return receiver;
  }

  public void setReceiver(Person receiver) {
    this.receiver = receiver;
  }

  public Boolean getRead() {
    return isRead;
  }

  public void setRead(Boolean read) {
    isRead = read;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Message message = (Message) o;

    if (id != null ? !id.equals(message.id) : message.id != null) {
      return false;
    }
    if (sender != null ? !sender.equals(message.sender) : message.sender != null) {
      return false;
    }
    if (receiver != null ? !receiver.equals(message.receiver) : message.receiver != null) {
      return false;
    }
    if (isRead != null ? !isRead.equals(message.isRead) : message.isRead != null) {
      return false;
    }
    if (date != null ? !date.equals(message.date) : message.date != null) {
      return false;
    }
    return content != null ? content.equals(message.content) : message.content == null;
  }

  @Override
  public int hashCode() {
    return Objects.hash(sender, receiver, isRead, date, content);
  }

}
