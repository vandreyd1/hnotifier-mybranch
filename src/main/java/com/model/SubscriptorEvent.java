package com.model;

import com.domain.LocalDateTimeAttributeConverter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SUBSCRIPTOR_EVENT")
public class SubscriptorEvent implements Serializable {

  @Id
  @SequenceGenerator(sequenceName = "subscriptor_id_sequence", name = "subscriptorIdSequence")
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscriptorIdSequence")
  @Column(name = "ID")
  private Integer id;
  //  @Id
  @ManyToOne
  @JoinColumn(name = "EVENT_ID")
  private Event event;

  //  @Id
  @ManyToOne
  @JoinColumn(name = "PERSON_ID")
  private Person person;

  @Column(name = "LAST_NOTIFICATION", columnDefinition = "timestamp")
  @Convert(converter = LocalDateTimeAttributeConverter.class)
  private LocalDateTime lastNotification;

  public SubscriptorEvent() {
  }

  public SubscriptorEvent(Event event, Person person) {
    this.event = event;
    this.person = person;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Event getEvent() {
    return event;
  }

  public void setEvent(Event event) {
    this.event = event;
  }

  public Person getPerson() {
    return person;
  }

  public void setPerson(Person person) {
    this.person = person;
  }

  public LocalDateTime getLastNotification() {
    return lastNotification;
  }

  public void setLastNotification(LocalDateTime lastNotification) {
    this.lastNotification = lastNotification;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }
    SubscriptorEvent that = (SubscriptorEvent) obj;
    return Objects.equals(event, that.event)
        && Objects.equals(person, that.person)
        && Objects.equals(lastNotification, that.lastNotification);
  }

  @Override
  public int hashCode() {
    return Objects.hash(event, person, lastNotification);
  }
}
