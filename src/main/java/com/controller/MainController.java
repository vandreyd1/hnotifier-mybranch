package com.controller;

import com.model.*;
import com.service.*;
import com.validator.*;
import java.security.*;
import javax.validation.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/")
public class MainController {

  private static String regRole;

  @Autowired
  PersonService personService;

  @Autowired
  PersonValidator personValidator;

  @Autowired
  SecurityService securityService;

  @RequestMapping(value = {"/"}, method = RequestMethod.GET)
  public String index() {
    return "redirect:/index";
  }

  @RequestMapping(value = {"/index"}, method = RequestMethod.GET)
  public String indexGet(Principal user, Model model) {
    model.addAttribute("name", "Hi, you're logged as " + user.getName());
    return "index/index";
  }

  @RequestMapping(value = {"/loginsuccess"}, method = RequestMethod.POST)
  public String loginSuccess() {
    return "index/loginsuccess";
  }

  @RequestMapping(value = "/registration", method = RequestMethod.GET, params = "role")
  public String registration(Model model,
      @RequestParam(value = "role", required = false, defaultValue = "student") String roleParam,
      Authentication authentication) {
    if (authentication != null && authentication.isAuthenticated()) {
      return "redirect:/index";
    }
    Person person = new Person();
    regRole = roleParam;
    model.addAttribute("userForm", person);
    return "auth/registration";
  }

  @RequestMapping(value = "/registration", method = RequestMethod.POST)
  public String registration(@ModelAttribute("userForm")
  @Valid
      Person userForm,
      BindingResult bindingResult) {
    personValidator.validate(userForm, bindingResult);
    if (bindingResult.hasErrors()) {
      return "auth/registration";
    }
    personService.save(userForm, regRole);
    securityService.autoLogin(userForm.getLogin(), userForm.getPasswordConfirm());
    return "redirect:/index";
  }

  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String login(Model model, String error, String logout, Authentication authentication) {
    if (authentication != null && authentication.isAuthenticated()) {
      return "redirect:/index";
    }
    if (error != null) {
      model.addAttribute("error", "Your username and password is invalid.");
    }
    if (logout != null) {
      model.addAttribute("logout", "You have been logged out successfully.");
    }
    return "auth/login";
  }

  @RequestMapping(value = "/login", method = RequestMethod.POST)
  public String loginRedir() {
    return "index/index";
  }

  @RequestMapping(value = {"/403"}, method = RequestMethod.GET)
  public String accesDenied(Principal user, Model model) {
    if (user != null) {
      model.addAttribute("msg", "Hi " + user.getName()
          + ", you do not have permission to access this page!");
    } else {
      model.addAttribute("msg",
          "You do not have permission to access this page!");
    }
    return "index/403";
  }
}
