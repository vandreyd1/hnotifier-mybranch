package com.controller;

import com.model.Email;
import com.service.EmailSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class EmailController {

    @Autowired
    EmailSendService emailSendService;

    @RequestMapping(value = "/mail", method = RequestMethod.GET)
    public String mail(Model model) {
        model.addAttribute("emailForm", new Email());
        return "mail";
    }

    @RequestMapping(value = "/mail", method = RequestMethod.POST)
    public String doSendEmail(@ModelAttribute("emailForm")
                                      Email emailForm, BindingResult bindingResult, Model model) {
        emailSendService.send(emailForm);
        return "redirect:/result";
    }
}
