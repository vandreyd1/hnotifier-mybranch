package com.controller;

import com.dto.EventTypeDto;
import com.service.EventTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@RestController
@RequestMapping("/event_type")
public class EventTypeController {

  @Autowired
  private EventTypeService eventTypeService;

  @RequestMapping(value = "/add", method = RequestMethod.PUT)
  public void addEventType(@RequestBody @Valid EventTypeDto dto) {
    eventTypeService.addEventType(dto);
  }

  @RequestMapping(value = "/update", method = RequestMethod.POST)
  public void update(@RequestBody @Valid EventTypeDto dto) {
    eventTypeService.updateEventType(dto);
  }

  @RequestMapping(value = "delete", method = RequestMethod.DELETE)
  public void delete(
      @RequestParam(value = "event_type_id")
      @NotNull(message = "event type id should be specified") Integer eventTypeId) {
    eventTypeService.deleteEventType(eventTypeId);
  }

  @RequestMapping(value = "/all")
  public List<EventTypeDto> findAll() {
    return eventTypeService.getEventTypes();
  }
}
