package com.controller;

import com.dto.LocationDto;
import com.service.LocationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/location")
public class LocationController {

  @Autowired
  private LocationService locationService;

  @RequestMapping(value = "/add", method = RequestMethod.PUT)
  public void addLocation(@RequestBody LocationDto locationDto) {
    locationService.addLocation(locationDto);
  }

  @RequestMapping(value = "/update", method = RequestMethod.POST)
  public void updateLocation(@RequestBody LocationDto locationDto) {
    locationService.updateLocation(locationDto);
  }

  @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
  public void deleteLocation(@RequestParam(value = "location_id") Integer locationId) {
    locationService.deleteLocation(locationId);
  }

  @RequestMapping(value = "/all")
  public List<LocationDto> findAll() {
    return locationService.getLocations();
  }
}
