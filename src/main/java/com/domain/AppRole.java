package com.domain;

public enum AppRole {
  ROLE_ANONYMOUS,
  ROLE_STUDENT,
  ROLE_TEACHER,
  ROLE_ADMIN
}
