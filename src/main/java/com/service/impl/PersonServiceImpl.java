package com.service.impl;

import com.dao.*;
import com.model.*;
import com.service.*;
import com.service.encryption.*;
import java.util.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.security.core.*;
import org.springframework.security.core.authority.*;
import org.springframework.security.core.userdetails.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

@Service("userDetailsService")
public class PersonServiceImpl implements PersonService {

  @Autowired
  PersonDAO personDAO;

  @Autowired
  Encrypt encrypt;

  @Autowired
  RoleDAO roleDAO;

  @Override
  public void save(Person person, String roleParam) {
    person.setPassword(encrypt.encode(person.getPassword()));

    Set<Role> roles = new HashSet<>();
    switch (roleParam) {
      case "student":
        roles.add(roleDAO.findAll().get(0)); // ROLE_STUDENT
//        roles.add(new Role(AppRole.ROLE_STUDENT));
        break;
      case "teacher":
        roles.add(roleDAO.findAll().get(3)); //ROLE_TEACHER
//        roles.add(new Role(AppRole.ROLE_TEACHER));
        break;
      default:
        roles.add(roleDAO.findAll().get(1)); //ROLE_ANONYMOUS
//        roles.add(new Role(AppRole.ROLE_ANONYMOUS));
        break;
    }
    person.setRoles(roles);

    personDAO.save(person);
  }

  @Override
  @Transactional(readOnly = true)
  public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

    Optional<Person> optionalPerson = personDAO.findByLogin(login);
    Person person;
    if (optionalPerson.isPresent()) {
      person = optionalPerson.get();
    } else {
      throw new UsernameNotFoundException("User not found");
    }
    Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
    for (Role role : person.getRoles()) {
      grantedAuthorities.add(new SimpleGrantedAuthority(role.getName().toString()));
    }
    return new User(person.getLogin(),
        person.getPassword(), grantedAuthorities);
  }

}
