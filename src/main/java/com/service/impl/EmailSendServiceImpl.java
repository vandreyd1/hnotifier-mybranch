package com.service.impl;

import com.model.Email;
import com.service.EmailSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSendServiceImpl implements EmailSendService{

    @Autowired
    JavaMailSender mailSender;

    @Autowired
    SimpleMailMessage mailMessage;

    @Override
    public void send(Email email) {
        mailMessage.setTo(email.getRecipientAddress());
        mailMessage.setSubject(email.getSubject());
        mailMessage.setText(email.getMessage());
        mailSender.send(mailMessage);
    }
}
