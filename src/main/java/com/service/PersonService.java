package com.service;

import com.model.*;
import org.springframework.security.core.userdetails.*;

public interface PersonService extends UserDetailsService {

  void save(Person person, String roleParam);

  UserDetails loadUserByUsername(String s) throws UsernameNotFoundException;
}
