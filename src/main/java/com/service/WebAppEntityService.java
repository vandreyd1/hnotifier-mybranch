package com.service;

import com.model.WebAppEntity;

import java.util.List;

public interface WebAppEntityService {

  List<WebAppEntity> getAllEntities();
}
