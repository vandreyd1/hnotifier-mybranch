package com.service;

import com.model.Email;
import org.springframework.mail.SimpleMailMessage;

public interface EmailSendService {

    void send(Email email);
}
