package com.service;

import com.dto.EventTypeDto;

import java.util.List;

public interface EventTypeService {

  void addEventType(EventTypeDto eventTypeDto);

  void updateEventType(EventTypeDto eventTypeDto);

  boolean deleteEventType(Integer eventTypeId);

  List<EventTypeDto> getEventTypes();
}
