package com.service.encryption;

import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
@Component
public class KeyGenerator {

    public static SecretKey getSecretKey() {

        try {
            javax.crypto.KeyGenerator generator = javax.crypto.KeyGenerator.getInstance("AES");
            generator.init(128);//to use 192/256-bit keys - download .JAR into {your jre.path}/lib/security
                                 //http://www.oracle.com/technetwork/java/javase/downloads/jce8-download-2133166.html
            return generator.generateKey();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
