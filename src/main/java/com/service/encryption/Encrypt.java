package com.service.encryption;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;

@Component
public class Encrypt implements PasswordEncoder {
    private static final byte[] keyValue = "HNotifier_Hillel".getBytes(StandardCharsets.UTF_8);

    private static Key generateKey() throws Exception {
        return new SecretKeySpec(keyValue, "AES");
    }

    @Override
    public String encode(CharSequence charSequence) {
        try {
            Key secretKey = generateKey();
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] byteCipherPassword = cipher.doFinal(charSequence.toString().getBytes());
            return Base64.encode(byteCipherPassword);
        } catch (NullPointerException e) {
            e.getMessage();
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean matches(CharSequence inputPassword, String encryptPassword) {
        String encryptInputPass = encode(inputPassword);
        return encryptInputPass != null && encryptPassword != null && encryptInputPass.equals(encryptPassword);
    }
}
