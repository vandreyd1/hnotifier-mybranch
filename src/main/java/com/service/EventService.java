package com.service;

import com.dto.EventDto;
import com.model.Event;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface EventService {

  void addEvent(EventDto eventDto);

  void updateEvent(EventDto eventDto);

  boolean deleteEvent(Integer eventId);

  List<EventDto> findByPeriodAndLocation(LocalDate from, LocalDate to, Integer locationId);
}
