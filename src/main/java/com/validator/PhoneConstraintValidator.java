package com.validator;


import com.annotations.*;
import javax.validation.*;

public class PhoneConstraintValidator implements ConstraintValidator<Phone, String> {
    @Override
    public void initialize(Phone phone) {}

    @Override
    public boolean isValid(String phone, ConstraintValidatorContext cxt) {
        if(phone == null) {
            return false;
        }
      return phone.matches("^\\+(?:[0-9] ?){6,14}[0-9]$");

    }
}
