package com.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.model.EventType;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class EventTypeDto {

  @JsonProperty(value = "ID")
  public Integer id;

  @NotBlank(message = "name shouldn't be empty")
  @JsonProperty(value = "NAME")
  public String name;

  public EventTypeDto() {
  }

  public EventTypeDto(EventType eventType) {
    this.id = eventType.getId();
    this.name = eventType.getName();
  }

  public EventType buildEventType() {
    EventType eventType = new EventType();
    eventType.setName(this.name);
    return eventType;
  }

  public EventType syncEventType(EventType eventType) {
    if (this.name != null) {
      eventType.setName(this.name);
    }
    return eventType;
  }
}
