package com.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.model.Event;

import java.time.Duration;
import java.time.LocalDateTime;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class EventDto {

  @JsonProperty(value = "ID")
  public Integer id;

  @JsonFormat(shape = Shape.STRING, pattern = "dd-MM-yyyy HH:mm")
  @JsonProperty(value = "DATETIME")
  @NotNull(message = "date and time should be specified")
  public LocalDateTime dateTime;

  @JsonProperty(value = "DURATION_MINUTE")
  @Min(value = 0, message = "duration cannot be negative")
  public Long minutes;

  @JsonProperty(value = "TEXT")
  public String text;

  @JsonProperty(value = "EVENT_TYPE_ID")
  public Integer eventTypeId;

  @JsonProperty(value = "LOCATION_ID")
  public Integer locationId;

  public EventDto() {
  }

  public EventDto(Event event) {
    this.id = event.getId();
    this.dateTime = event.getDateTime();
    this.minutes = event.getDuration() == null ? 0 : event.getDuration().toMinutes();
    this.text = event.getText();
    this.eventTypeId = event.getEventType().getId();
    this.locationId = event.getLocation().getId();
  }

  public Event buildEvent() {
    Event event = new Event();
    event.setText(this.text);
    event.setDateTime(this.dateTime);
    event.setDuration(Duration.ofMinutes(this.minutes));
    return event;
  }

  public Event syncEvent(Event event) {
    if (this.text != null) {
      event.setText(this.text);
    }
    if (this.minutes != null) {
      event.setDuration(Duration.ofMinutes(this.minutes));
    }
    if (this.dateTime != null) {
      event.setDateTime(this.dateTime);
    }
    return event;
  }
}
